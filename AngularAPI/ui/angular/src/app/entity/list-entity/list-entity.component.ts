import { DomElementSchemaRegistry } from '@angular/compiler';
import { Component, Input, OnInit } from '@angular/core';
import { EntityService } from 'src/app/entity.service';

@Component({
  selector: 'app-list-entity',
  templateUrl: './list-entity.component.html',
  styleUrls: ['./list-entity.component.css']
})
export class ListEntityComponent implements OnInit {

  
  EntityList:any=[];
 
  //! shows that will have a value at runtime
  ModalTitle!:string;
  ActivateAddEditEntComp:boolean=false;
  @Input() entity:any;
  EntityNameFilter:string="";

  EntityListWithoutFilter:any=[];


  constructor(private service:EntityService) { }

  ngOnInit(): void {
    this.refreshEntList();
    
  }

  addClick(){
    // ky subjekt do marre id 0 per te kuptuar qe jemi duke shtuar nje te ri
    this.entity={
      id:0,
      name:""
    }
    this.ModalTitle="Shto subjekt";
    this.ActivateAddEditEntComp=true;

  }

  editClick(item:any){
    this.entity=item;
    this.ModalTitle="Modifiko";
    this.ActivateAddEditEntComp=true;
   
  }

  deleteClick(item:any){
    if(confirm('Are your sure??')){
     this.service.deleteEntity(item.id).subscribe(data=>{
        alert(data.toString());
        this.refreshEntList();

      });
    }
  }

  closeClick(){
    this.ActivateAddEditEntComp=false;
    this.refreshEntList();
  }

  refreshEntList(){
    this.service.getEntityList().subscribe(
      data=>{
      this.EntityList=data;
      this.EntityListWithoutFilter=data;
    });
  }

  FilterEntity(){
    var EntityNameFilter=this.EntityNameFilter;
    this.EntityList=this.EntityListWithoutFilter.filter(function(el:any){
      return el.name.toString().toLowerCase().includes(
        EntityNameFilter.toString().trim().toLowerCase()
      )
    });
  }

  sortResult(prop:any,asc:any){
    this.EntityList=this.EntityListWithoutFilter.sort(function(a:any,b:any){
      if(asc){
        return (a[prop]>b[prop])?1 : ((a[prop]<b[prop]) ?-1 :0);
        
      }else{
        return (b[prop]>a[prop])?1 : ((b[prop]<a[prop]) ?-1 :0);

      }
    });

  }




}
