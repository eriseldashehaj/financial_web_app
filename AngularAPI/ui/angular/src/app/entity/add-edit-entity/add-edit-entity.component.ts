import { Component, Input, OnInit } from '@angular/core';
import { EntityService } from 'src/app/entity.service';

@Component({
  selector: 'app-add-edit-entity',
  templateUrl: './add-edit-entity.component.html',
  styleUrls: ['./add-edit-entity.component.css']
})
export class AddEditEntityComponent implements OnInit {

  constructor(private service:EntityService) { }


  @Input() entity:any;
  id!:string;
  name!:string;
  entity_nbr!:string;
  @Input() org_type:any;
  activity_field!:string;
  address!:string;
  phone!:string;
  email!:string;
  @Input() reg_country:any;



  ngOnInit(): void {
    this.id=this.entity.id;
    this.name=this.entity.name;
    this.address=this.entity.address;
    this.phone=this.entity.phone;
    this.entity_nbr=this.entity.entity_nbr;
    this.org_type=this.entity.org_type.name;
    this.activity_field=this.entity.activity_field;
    this.email=this.entity.email;
    this.reg_country=this.entity.reg_country.name;

  }

  //inicializojme atributet e objekti ne formatin json
  addEntity(){
  
    var val={ 
            id:this.id,
            name:this.name,
            entity_nbr:this.entity_nbr,
            reg_country:this.reg_country,
            org_type:this.org_type,
            phone:this.phone,
            address:this.address,    
            activity_field:this.activity_field,
            email:this.email,

    }

    this.service.addEntity(val).subscribe(res=>{
      alert(res.toString());
    });

  }

  updateEntity(){
    var val={ 
      id:this.id,
      name:this.name,
      entity_nbr:this.entity_nbr,
      reg_country:this.reg_country,
      org_type:this.org_type,
      phone:this.phone,
      address:this.address,    
      activity_field:this.activity_field,
      email:this.email,

}

this.service.updateEntity(val).subscribe(res=>{
alert(res.toString());
});

  }

}
