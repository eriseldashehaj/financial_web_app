
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(private cookieService:CookieService,
              private router:Router) { }

  ngOnInit(): void {
    const token=this.cookieService.get('financial-token')
    if(!token){
      this.router.navigate(['/login/']);

    }else{
      this.router.navigate(['/entity/']);
    }
  }

  logout(){
    this.cookieService.delete('financial-token');
    this.router.navigate(['/login']);
  }

}
