import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { AuthserviceService } from './auth/authservice.service';


@Injectable({
  providedIn: 'root'
})
export class EntityService {
  readonly APIUrl="http://127.0.0.1:8000";
  token=this.cookieService.get('financial-token');
  headers=new HttpHeaders({
    'Content-Type':'application/json',
    
  });
  
 
  constructor(private http: HttpClient, 
              private cookieService:CookieService,
              private authService:AuthserviceService) { }


  getEntityList():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + '/entities/',{headers:this.authService.getAuthHeaders()} );

  }

  addEntity(val:any){
    return this.http.post(this.APIUrl + '/entity/' , val, {headers:this.authService.getAuthHeaders()} );

  }


  updateEntity(val:any){
    return this.http.put(this.APIUrl + '/entity/' , val, {headers:this.authService.getAuthHeaders()} );

  }

  deleteEntity(val:any){
    return this.http.delete(this.APIUrl + '/entity/'+ val, {headers:this.authService.getAuthHeaders()} );

  }


}
