import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';


@Injectable({
  providedIn: 'root'
})
export class AuthserviceService {
  readonly APIUrl="http://127.0.0.1:8000";
  token=this.cookieService.get('financial-token')
  headers=new HttpHeaders({
    'Content-Type':'application/json',
  });

  constructor(private http:HttpClient,
              private cookieService:CookieService) { 

                }

  loginUser(authData:any){
    const body=JSON.stringify(authData)
    return this.http.post(this.APIUrl+ '/auth/', body,{headers:this.headers});

  }

  // everytime a request is done to backend must contain also the auth token
  getAuthHeaders(){
    const token=this.cookieService.get('financial-token')
    return new HttpHeaders({
      'Content-Type':'application/json',
      Authorization:`Token ${token}`,
    });

  }

  
}
