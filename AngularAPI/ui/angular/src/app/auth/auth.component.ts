import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { AuthserviceService } from './authservice.service';
import {Router} from '@angular/router';



@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {


authForm=new FormGroup({
  username:new FormControl(''),
  password:new FormControl('')
});

  constructor(private authservice:AuthserviceService,
              private cookieService: CookieService,
              private router:Router) { }

  ngOnInit(): void {

    const financialToken=this.cookieService.get('financial-token')
    if(financialToken){
      this.router.navigate(['/entity/']);

    }
    
    
  }

  saveForm(){
    this.authservice.loginUser(this.authForm.value).subscribe(
     (result: any)=>{
        this.cookieService.set('financial-token', result.token);
        this.router.navigate(['/entity/']);
      },
      error=>console.log(error)
    );
  }

  


}
