import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListEntityComponent } from './entity/list-entity/list-entity.component';
import { AddEditEntityComponent } from './entity/add-edit-entity/add-edit-entity.component';
import { EntityComponent } from './entity/entity.component';
import { AuthComponent } from './auth/auth.component';
import { MainComponent } from './main/main.component';

const routes: Routes = [

{path:'entity', component:EntityComponent},
{path:'login', component:AuthComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
