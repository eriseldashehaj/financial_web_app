import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EntityComponent } from './entity/entity.component';
import { ListEntityComponent } from './entity/list-entity/list-entity.component';
import { AddEditEntityComponent } from './entity/add-edit-entity/add-edit-entity.component';
import { EntityService } from './entity.service';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthComponent } from './auth/auth.component';
import { CookieService } from 'ngx-cookie-service';
import { MainComponent } from './main/main.component';

@NgModule({
  declarations: [
    AppComponent,
    EntityComponent,
    ListEntityComponent,
    AddEditEntityComponent,
    AuthComponent,
    MainComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  providers: [EntityService, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
