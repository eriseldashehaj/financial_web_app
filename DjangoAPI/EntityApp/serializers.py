from rest_framework import serializers
from EntityApp.models import Entity, RegisteredCountry, OrganizationType
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

class RegisteredCountrySerializer(serializers.ModelSerializer):
    class Meta:
        model=RegisteredCountry
        fields='__all__'


class OrganizationTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model=OrganizationType
        fields='__all__'



class EntitySerializer(serializers.ModelSerializer):
    class Meta:
        model=Entity
        fields='__all__'
        ###we add the depth feature and it shows the related models json
        depth=1

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model=User
        fields=('id', 'username', 'password')
        #password will not be able to see and it will be required if we want to register a user
        extra_kwargs={'password':{'write_only':True, 'required':True}}

    def create(self, validated_data):
        user=User.objects.create_user(**validated_data)
        ### Create also the Token after registering user
        token=Token.objects.create(user=user)
        return user
        
