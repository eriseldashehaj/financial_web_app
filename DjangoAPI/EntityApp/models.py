from django.db import models
from django.contrib.auth.models import User

class RegisteredCountry(models.Model):
    name=models.CharField(max_length=30)
    created_date=models.DateTimeField(auto_now=True)
    updated_date=models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Vendi_Regjistrimit"

class OrganizationType(models.Model):
    name=models.CharField(max_length=100)
    created_date=models.DateTimeField(auto_now=True)
    updated_date=models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Lloji_Organizates"

class Entity(models.Model):
    name=models.CharField(max_length=50)
    reg_country=models.ForeignKey(RegisteredCountry,blank=True, null=True,on_delete=models.CASCADE, related_name='country_entities')
    entity_nbr=models.CharField(max_length=10, unique=True)
    org_type=models.ForeignKey(OrganizationType,blank=True, null=True,on_delete=models.CASCADE, related_name='org_type_entities')
    activity_field=models.CharField(max_length=100)
    address=models.TextField(max_length=100)
    phone=models.IntegerField()
    user=models.ForeignKey(User, blank=True, null=True,on_delete=models.CASCADE, related_name='user_entities')
    email=models.EmailField(max_length=255, unique=True)
    created_date=models.DateTimeField(auto_now=True)
    updated_date=models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Subjekte"



