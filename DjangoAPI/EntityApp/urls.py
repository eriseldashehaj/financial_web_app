from django.conf.urls import include
from django.urls.conf import path
from EntityApp import views
from rest_framework.routers import DefaultRouter


router=DefaultRouter()
router.register('entities', views.EntityViewSet)
router.register('users', views.UserViewSet)


urlpatterns=[
    path('entity/', views.entityApi),
    path('', include(router.urls)),
]