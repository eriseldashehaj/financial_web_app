from django.contrib import admin
from . import models


admin.site.register(models.Entity)
admin.site.register(models.RegisteredCountry)
admin.site.register(models.OrganizationType)