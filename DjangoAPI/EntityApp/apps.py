from django.apps import AppConfig


class EntityappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'EntityApp'
