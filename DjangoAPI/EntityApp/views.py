from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from EntityApp.models import Entity
from EntityApp.serializers import EntitySerializer, UserSerializer
from django.http.response import JsonResponse
from rest_framework.authentication import TokenAuthentication
from rest_framework import generics, viewsets
from rest_framework.permissions import SAFE_METHODS, IsAdminUser, IsAuthenticated, BasePermission


class UserPostWritePermission(BasePermission):
    message='Lista dhe shtimi i Subjekteve eshte e drejte vetem e krijuesit.'

    def has_object_permission(self, request, view, obj):
    
        if request.method in SAFE_METHODS:
            return True
        ### if the user is the author he can do all the GET,POST,PUT,DELETE methods
        if obj.author == request.user:
            return True
        return False

class UserViewSet(viewsets.ModelViewSet):
    serializer_class=UserSerializer
    queryset=User.objects.all()
     #Telling this view needs to be token authenticated before showing
    #otherwise won't be shown
    authentication_classes=(TokenAuthentication,)
    ### the total list of users is only viewed by admin user
    permission_classes=(IsAuthenticated, IsAdminUser)



class EntityViewSet(viewsets.ModelViewSet):
    permission_classes=[IsAuthenticated,IsAdminUser,UserPostWritePermission]
    serializer_class=EntitySerializer
    queryset=Entity.objects.all()
    authentication_classes=(TokenAuthentication,)


@csrf_exempt
def entityApi(request, id=0):
    if request.method=='GET':
        entities=Entity.objects.all()
        entities_serializer=EntitySerializer(entities,many=True)
        #safe=False ---we are trying to get Json but we are fine 
        # if it happen any issues meaning just return it whatever the matter
        return JsonResponse(entities_serializer.data,safe=False)
    elif request.method=='POST':
        entity_data=JSONParser().parse(request)
        entity_serializer=EntitySerializer(data=entity_data)
        if entity_serializer.is_valid():
            entity_serializer.save()
            return JsonResponse("Subjekti u shtua me sukses!", safe=False)

        return JsonResponse("Subjekti nuk u shtua!", safe=False)

    elif request.method=='PUT':
        entity_data=JSONParser().parse(request)
        entity=Entity.objects.get(id=entity_data['id'])
        entity_serializer=EntitySerializer(entity, data=entity_data)
        if entity_serializer.is_valid():
            entity_serializer.save()
            return JsonResponse("Te dhenat e subjektit u perditesuan!", safe=False)
        return JsonResponse("Te dhenat e subjektit nuk arriten te perditesoheshin!", safe=False)

    elif request.method=='DELETE':
        entity=Entity.objects.get(id=id)
        entity.delete()
        return JsonResponse("Subjekti u fshi me sukses!", safe=False)